---
title: Thoughts on fast.ai chapter 4
date: 2020-10-27
---

In chapter 4 fast.ai team introduces fundamental concepts such as

* Weights (biases)
* Loss function
* Optimization loops and gradient descent
* Vectorization and tensors
* Neural network layers and activations

## Thoughts on chapter

TBH the chapter was a bit messy. I was constantly confused by authors introducing new variables and syntax and had to manually keep track of what goes on with data behind the scenes. For example, at some point authors quietly introduce a PyTorch function `unsqueeze` without mentioning what it does. I was also a little bit confused by a concept of `Learner` from their own fast.ai library. A `Learner` is a class/function that iterates through epochs with given data and loss function. I can't believe this isn't implemented in PyTorch (TensorFlow has it) so a bit of trivia on why did they redo it would be helpful.

I really enjoyed short section on layers as it clearly stated why NN are such a good way to solve complex problems. Authors stated in a very straightforward ways that weights are linear functions and activation layers applied to them are what maked NN non-linear. Explanation of gradient decent was also good.

## Questionnaire

I decided that I won't answer all questions, but only interesting ones

#### Explain how the "pixel similarity" approach to classifying digits works.

Pixel similarity calculates difference b/w each element of an array. Difference calculated either by using L1 or L2 norm.

#### What is a "rank-3 tensor"?

A tensor with three axes. Authors have a good example that a vector of 3 dimensions won't be a tensor.

#### What is the difference between tensor rank and shape? How do you get the rank from the shape?

By calculating length of a shape

#### Create a 3×3 tensor or array containing the numbers from 1 to 9. Double it. Select the bottom-right four numbers.

```
t = tensor([i for i in range(1, 10)]).reshape((3,3))
t *= 2
t[1:,1:]
```

#### What are the seven steps in SGD for machine learning?

1. Init parameters
2. Calculate predictions
3. Calculate loss
4. Calculate gradient
5. Update weigths
6. Repeat
7. End

#### What are the seven steps in SGD for machine learning?

Loss is a function that is used in GD to automatically asess if current iteration was useful for an end goal or not

#### What is a "gradient"?

Gradient is a function identifying a slope of an underlying surface

#### Why can't we use accuracy as a loss function?

It isn't sensitive enough

#### What is the function to calculate new weights using a learning rate?

`train_epoch` ?

#### What does the DataLoader class do?

It loads Data in a specified way and returns an iterator. For example in this chapter authors use it to break data into batches and shuffle data.   

#### Write pseudocode showing the basic steps taken in each epoch for SGD.

````
model = load_model()
lr = load_lr()
weights = get_params()
new_preds = calculate_preds(weights, model, backprop=True)
loss = calculate_loss(new_preds, ground_truth) 
# Loss is something like sum(sqrt((sigmoid(a) - b)**2)), where a and b
# are 1 or 0 for each tensor. 
if loss < current_loss:
    grads = new_preds.backprop()
    weights -= grads * lr

````

#### Create a function that, if passed two arguments [1,2,3,4] and 'abcd', returns [(1, 'a'), (2, 'b'), (3, 'c'), (4, 'd')]. What is special about that output data structure?

````
list(zip([1,2,3,4], ['a', 'b', 'c', 'd']))
````

That's a format we use for setting `X -> y` relationship in our models

#### What does view do in PyTorch?

It reshapes a tensor into specified shape

#### What does the backward method do?

Calculates gradient values at the point of a current iteration

#### What information do we have to pass to Learner?

* Dataset
* Model
* Loss function
* Optimisation function
* Metrics

#### Show Python or pseudocode for the basic steps of a training loop.

???
````
calc_grad(x, y, model)
for p in params:
    p.data -= p.grad*lr
    p.grad.zero_()
````
