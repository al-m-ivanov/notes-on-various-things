---
title: First post (fast.ai lesson 2)
date: 2020-10-25
---

Ok. I started to learm more on deep learning using fast.ai course. They say you should write your notes after each unit and that sounds as the right thing to do.

## First impressions on the course

So far it's really easy and fun. Compared to Deeplearning course by Andrew Ng there's no theory at all. In the first two units we've learned various library-specific gimmicks (very useful, but that isn't deep learning). Instead of cats vs dogs classifier authors teach reader to build a bear classifier. An important thing is they teach us a gimmick to download images automatically from Bing image search that could be very useful in future.

Authors advice to choose a project from the beginning and use techniques from the lesson to work on the project. A great idea, but what to choose? I need to make a decision this week.

## My answers to chapter 2 questions

#### Provide an example of where the bear classification model might work poorly in production, due to structural or style differences in the training data.

It won't work on images that won't appear at the top of the search results:
* Poor qulaity images
* Images from behind
* Night photos
* Photos made from strange angles
* Different input formats

#### Where do text models currently have a major deficiency?

Text models have no sure way to asess the correctness of the generated text. It may look good, but there' no guarantee if it has a meaning or if it's correct.

#### What are possible negative societal implications of text generation models?

It's easier to generate specific text with a selected point of views than to detect it. This opens possibilieties for managing people opinions through social networks or other channels. For example email newsletters.

#### In situations where a model might make mistakes, and those mistakes could be harmful, what is a good alternative to automating a process?

* First roll out a non-automated model that has every result reviewed by a human. Retrain model using human corrections until a sufficiently good result is achieved
* Roll out a limited model that won't be harmful. For example, allow self-driving car to navigate on a specific straight sector of a highway having human operator onboard. Test it for some time to sort errors out
* Gradually increase the rollout scope. I. e. allow the car from the previous example to change lanes, next to navigate on emty roads with turns etc.

#### What kind of tabular data is deep learning particularly good at?

Data with many feautures; data with categorial feautures

#### What's a key downside of directly using a deep learning model for recommendation systems?

It'd recommend items a person most likely already familiar with. The key is to use sales metric.

#### What are the steps of the Drivetrain Approach?

* Set the clear objective (create a drone that can detect trespassers)
* Enumerate actions that you can take (detect people among other objects; identify people with access to territory)
* Identify new data we would need (a dataset of people on a territory similar to ours; photos of people with access in different poses)
* Only then build a model

#### How do the steps of the Drivetrain Approach map to a recommendation system?
* Set the objective: increase sales
* Actions: Order of recommended items
* New data: Views of items vs purchases, then a/b-testing
* Model: two conditional models. One with probability of purchase with implemented recommendations and another without recommendations. The difference between models will be the actual result.

#### Create an image recognition model using data you curate, and deploy it on the web.

TBD

#### What is DataLoaders?

A gimmick from fast.ai that easily allows a user to load data in a specific format with inferred labels and transformations.

#### What four things do we need to tell fastai to create DataLoaders?

* Independent and dependent variables
* A function to load data
* A way to split data
* A fucntion for labelling data

#### What does the splitter parameter to DataBlock do?

Splits data according to parameters set by user


#### How do we ensure a random split always gives the same validation set?

We set a seed

#### What letters are often used to signify the independent and dependent variables?

X and y

#### What's the difference between the crop, pad, and squish resize approaches? When might you choose one over the others?

Crop drops some parts of the images to fit a size, pad fills image with zeros to fit a size, squish resizes it disregarding proportions. Well, authors say in production we always use parts of the image that fit the specified size and simply break one image into several if it doesn't fit

#### What is data augmentation? Why is it needed?

It's needed to increase dataset. We transorm data to generate new examples. For example mirror an image.

#### What is the difference between item_tfms and batch_tfms?

Transform on an item vs transform on a batch

#### What is a confusion matrix?

A way to see how much data was misclassified vs classified correctly

#### What does export save?

A pickled model

#### What is it called when we use a model for getting predictions, instead of training?

Using a Model?

#### When might you want to use CPU for deployment? When might GPU be better?

CPU is fine for low-latency applications. GPU - for high-latency or performance-critical ones

#### What are the downsides of deploying your app to a server, instead of to a client (or edge) device such as a phone or PC?

Network latency. Network availability

#### What is "out-of-domain data"?

Data of a target that wasn't reflected in a dataset. For example photos taken in the rain

#### What is "domain shift"?

A situation where data given to the model changes over time that renders initial model obsolete

#### What are the three steps in the deployment process?

Manual process -> Limited Deployment -> Gradual rollout